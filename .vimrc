" -----------------------------------------------------------------------------
" Cursor
" -----------------------------------------------------------------------------

set cursorline

" -----------------------------------------------------------------------------
" Paste
" -----------------------------------------------------------------------------

set pastetoggle=<F2>

" -----------------------------------------------------------------------------
" Compatibility
" -----------------------------------------------------------------------------

" No Vi-compatibility.
set nocompatible

" When making a change to one line, don't redisplay the line, but put a "$" at
" the end of the changed text.
set cpoptions+=$

" Use both Unix and DOS file formats, but favor the Unix one for new files.
set fileformats=unix,dos

" Convert backward slashes "\" to forward slashes "/" when expanding file
" names.
" set shellslash

" NOTE: Do not set "shellslash" if the current "shell" does not refer to the
" Unix shell. For instance, if you are on Windows and you didn't change the
" "shell" variable, then your current shell is certainly "cmd.exe" by default.
" Setting "shellslash" in this case is devastating. For example, the
" "shellescape()" function will enclose it's arguments in single quotes "'"
" which are not recognized by "cmd.exe" as one might expect.

" Use Unix convention for personal runtime directory on Windows.
if has('win32') || has('win64')
  set runtimepath^=~/.vim
  set runtimepath+=~/.vim/after
endif

set runtimepath^=~/vim-configuration/.vim
set runtimepath+=~/vim-configuration/.vim/after

" -----------------------------------------------------------------------------
" Ctags
" -----------------------------------------------------------------------------

set tags=./tags;/

" -----------------------------------------------------------------------------
" Encoding
" -----------------------------------------------------------------------------

" Stick with the UTF-8 encoding.
if has('multi_byte')
  " Encoding used for the terminal.
  if empty(&termencoding)
    let &termencoding = &encoding
  endif

  " Encoding used in buffers, registers, strings in expressions, "viminfo"
  " file, etc.
  set encoding=utf-8

  " Encoding used for writing files.
  setglobal fileencoding=utf-8
endif

" -----------------------------------------------------------------------------
" GUI / Look & Feel
" -----------------------------------------------------------------------------

" Settings for GUI mode.
if has('gui_running')
  " 1) Make the visually highlighted text be available for pasting into other
  " applications as well as into Vim itself. 2) Add tab pages when indicated
  " with "showtabline". 3) Right scrollbar is always present. 4) Left
  " scrollbar is present when there are vertically split windows.
  set guioptions=aerL

  " Configure font and its size.
  set guifont=Monospace\ 12
endif

" Display line numbers.
set number

" Enhance completion in Command-Line Mode.
set wildmenu

" Ignore non-text or back-up files.
set wildignore=*.o,*.obj,*.a,*.lib,*.so,*.dll,*.exe,*.pyc,*.class,*.swp,*~

" The title of the window is determined by the "titlestring" variable.
set title

let &titlestring = expand('$USERNAME') . '@' . hostname()
    \ . ' : ' . '%{fnamemodify(getcwd(), ":p:~:h")}'
    \ . ' > ' . '%{expand("%:p:~")}'

" Show status line, even if there is only one window.
set laststatus=2

" Customize status line.
set statusline+=%t
set statusline+=\ \|
set statusline+=\ %y
set statusline+=[%{&fileencoding}]
set statusline+=%{&bomb?'[BOM]':''}
set statusline+=[%{&fileformat}]
set statusline+=%r
set statusline+=%m
set statusline+=\ \|
set statusline+=\ #%n
set statusline+=\ \|
set statusline+=\ [%c/%{&textwidth}]:[%l/%L][%p%%]
"set statusline+=\ \|
"set statusline+=\ [%b][0x%B]
"set statusline+=\ \|
"set statusline+=\ [%{mode(1)}]
set statusline+=%{&paste?'[paste]':''}

" Show completion pop-up menu even when there is only 1 match.
set completeopt=menuone

" Disable folding.
set nofoldenable

" Indicate matching brackets when cursor is over them.
set showmatch

" New windows open to the right of the current one.
" set splitright

" -----------------------------------------------------------------------------
" Navigation
" -----------------------------------------------------------------------------

" Allow specified keys that move the cursor left/right to move to the
" previous/next line when the cursor is on the first/last character in the line
set whichwrap=b,s

" Keep some lines visible above and below the cursor while scrolling.
set scrolloff=7

" Allow the cursor to be positioned where there is no actual character, but
" only in Blockwise Visual Mode.
set virtualedit=block

" -----------------------------------------------------------------------------
" Editing
" -----------------------------------------------------------------------------

" Make "<BS>" and "<Del>" behavior less surprising.
set backspace=eol,start,indent

" Do not insert 2 spaces after ".", "?", and "!" when join command is used.
set nojoinspaces

" -----------------------------------------------------------------------------
" Indentation
" -----------------------------------------------------------------------------

" Number of spaces that a tab counts for when displaying a file.
set tabstop=2

" Number of spaces that a tab counts for when performing editing operations,
" like inserting tab or using "<BS>".
set softtabstop=2

" Number of spaces to use for each step of (automatic) indentation.
set shiftwidth=2

" Use spaces instead of tabs.
set expandtab

" Smart handling of indentation in the beginning of the line.
set smarttab

" Perform automatic indentation when starting the new line.
set autoindent

" Copy whatever characters were used for indenting on the existing line to the
" new line.
set copyindent

" -----------------------------------------------------------------------------
" Wrapping
" -----------------------------------------------------------------------------

" Disable displaying of non-printable characters. Required for "wrap" to work
" properly.
set nolist

" Enable soft text wrapping.
" set wrap

" Wrap at a character in "breakat" rather than at the last character that fits
" on the screen.
" set linebreak

" Enable hard text wrapping with a limit of 119 columns per line. See "formatoptions" for details.
set textwidth=119

" 1) Auto-wrap text using "textwidth". 2) Auto-insert comment leader after
" hitting "<CR>". 3) Allow formatting of comments with "gq". 4) Recognize
" numbered lists.
"set formatoptions=t,r,q,n 
set formatoptions=r,q,n

" -----------------------------------------------------------------------------
" Search / Regular Expressions
" -----------------------------------------------------------------------------

" Wrap around the end of the file when searching.
set wrapscan

" Highlight search matches.
set hlsearch

" Search as you type.
set incsearch

" Ignore case in the search pattern.
set ignorecase

" Override the "ignorecase" option if the search pattern contains uppercase
" letters.
set smartcase

" In regular expressions, treat most characters literally, while require
" certain ones to be preceded with backward slash "\" in order to gain special
" meaning.
set magic

" -----------------------------------------------------------------------------
" Spell Checking
" -----------------------------------------------------------------------------

" Enable spell checking.
" set spell

" Use the following dictionaries for spell checking.
set spelllang=en_us

" -----------------------------------------------------------------------------
" Effects & Sounds
" -----------------------------------------------------------------------------

" Enable bells for errors containing messages.
set errorbells

" Don't use screen flash instead of bell.
set novisualbell

" -----------------------------------------------------------------------------
" History
" -----------------------------------------------------------------------------

" The number of entries to remember for the history of command-line commands
" and search patterns.
set history=100

" Restore buffers from previous session. Only works if Vim is started without
" file name argument.
set viminfo^=%

" -----------------------------------------------------------------------------
" Backup
" -----------------------------------------------------------------------------

set writebackup

set backup

set backupcopy=yes

set backupskip=

set backupdir=~/.backup

autocmd BufWritePre * let &backupext = '~@'
    \ . substitute(expand('%:p:h'), '[\\/:]', '%', 'g')

set swapfile

set updatetime=2000

set directory=~/.swap//

" -----------------------------------------------------------------------------
" External Utilities
" -----------------------------------------------------------------------------

" Make "grep" program to always generate a file name.
" set grepprg=grep\ -nH\ $*

" Use "vimgrep".
set grepprg=internal

" -----------------------------------------------------------------------------
" "pathogen"
" -----------------------------------------------------------------------------

" Recursively populate the "runtimepath" variable with subdirectories of the
" "~/.vim/plugins/" directory.
call pathogen#infect('plugins')

" NOTE: It is important that the "pathogen#infect()" function is called before
" the ":filetype on" command.

" Invoke ":helptags" on all "doc" subdirectories in the "runtimepath" variable.
call pathogen#helptags()

" -----------------------------------------------------------------------------
" File Type Detection
" -----------------------------------------------------------------------------

" 1) Enable file type detection. 2) Enable loading the plugin files for
" specific file types. 3) Enable loading the indent files for specific file
" types.
filetype plugin indent on

" If no keywords are found to distinguish between TeX, ConTeXt, and LaTeX, then
" fall back to the LaTeX format by default.
let g:tex_flavor = 'latex'

" -----------------------------------------------------------------------------
" Syntax Highlighting / Color Scheme
" -----------------------------------------------------------------------------

" Enable syntax highlighting.
syntax on

set t_Co=256

" NOTE: The file type detection is also used for syntax highlighting. If the
" ":syntax on" command is used, the ":filetype on" command is invoked
" implicitly.

" Use dark background.
set background=dark

" Set color scheme.
colorscheme torte

" Mark 80 columns as a line limit with a vertical line.
set colorcolumn=120

" highlight Function gui=bold

"highlight ColorColumn guibg=#880000

highlight TrailingWhitespace guibg=#FF0000
highlight ExtraWhitespace    guibg=#00343f

autocmd Syntax * call matchadd('TrailingWhitespace', '\s\+$')
autocmd Syntax * call matchadd('ExtraWhitespace',    '\S\+\zs\s\{2,}\ze\S\+')

" -----------------------------------------------------------------------------
" Hooks
" -----------------------------------------------------------------------------

autocmd FileWritePre * call Trim()
" autocmd FileType python autocmd BufWritePre <buffer> call Indent()

" -----------------------------------------------------------------------------
" Unmapped Key Bindings
" -----------------------------------------------------------------------------

"nnoremap <Up> <Nop>
"inoremap <Up> <Nop>
"vnoremap <Up> <Nop>
"onoremap <Up> <Nop>

"nnoremap <S-Up> <Nop>
"inoremap <S-Up> <Nop>
"vnoremap <S-Up> <Nop>
"onoremap <S-Up> <Nop>

"nnoremap <Down> <Nop>
"inoremap <Down> <Nop>
"vnoremap <Down> <Nop>
"onoremap <Down> <Nop>

"nnoremap <S-Down> <Nop>
"inoremap <S-Down> <Nop>
"vnoremap <S-Down> <Nop>
"onoremap <S-Down> <Nop>

"nnoremap <Left> <Nop>
"inoremap <Left> <Nop>
"vnoremap <Left> <Nop>
"onoremap <Left> <Nop>

"nnoremap <S-Left> <Nop>
"inoremap <S-Left> <Nop>
"vnoremap <S-Left> <Nop>
"onoremap <S-Left> <Nop>

"nnoremap <Right> <Nop>
"inoremap <Right> <Nop>
"vnoremap <Right> <Nop>
"onoremap <Right> <Nop>

"nnoremap <S-Right> <Nop>
"inoremap <S-Right> <Nop>
"vnoremap <S-Right> <Nop>
"onoremap <S-Right> <Nop>

nnoremap h <Nop>
vnoremap h <Nop>
onoremap h <Nop>

nnoremap H <Nop>
vnoremap H <Nop>
onoremap H <Nop>

nnoremap k <Nop>
vnoremap k <Nop>
onoremap k <Nop>

nnoremap gk <Nop>
vnoremap gk <Nop>
onoremap gk <Nop>

nnoremap K <Nop>
vnoremap K <Nop>
onoremap K <Nop>

nnoremap j <Nop>
vnoremap j <Nop>
onoremap j <Nop>

nnoremap gj <Nop>
vnoremap gj <Nop>
onoremap gj <Nop>

nnoremap J <Nop>
vnoremap J <Nop>

nnoremap l <Nop>
vnoremap l <Nop>
onoremap l <Nop>

nnoremap L <Nop>
vnoremap L <Nop>
onoremap L <Nop>

nnoremap w <Nop>
vnoremap w <Nop>
onoremap w <Nop>

nnoremap W <Nop>
vnoremap W <Nop>
onoremap W <Nop>

nnoremap b <Nop>
vnoremap b <Nop>
onoremap b <Nop>

nnoremap B <Nop>
vnoremap B <Nop>
onoremap B <Nop>

nnoremap e <Nop>
vnoremap e <Nop>
onoremap e <Nop>

nnoremap E <Nop>
vnoremap E <Nop>
onoremap E <Nop>

nnoremap ge <Nop>
vnoremap ge <Nop>
onoremap ge <Nop>

nnoremap gE <Nop>
vnoremap gE <Nop>
onoremap gE <Nop>

nnoremap d <Nop>
vnoremap d <Nop>
onoremap d <Nop>

nnoremap D <Nop>
vnoremap D <Nop>

nnoremap s <Nop>
vnoremap s <Nop>

nnoremap S <Nop>
vnoremap S <Nop>

nnoremap i <Nop>

nnoremap I <Nop>

nnoremap a <Nop>

nnoremap A <Nop>

nnoremap q <Nop>

nnoremap Q <Nop>

nnoremap zz <Nop>
vnoremap zz <Nop>
onoremap zz <Nop>

noremap ZZ <Nop>
noremap ZQ <Nop>

nnoremap <C-b> <Nop>
inoremap <C-b> <Nop>
vnoremap <C-b> <Nop>
onoremap <C-b> <Nop>

nnoremap <C-f> <Nop>
inoremap <C-f> <Nop>
vnoremap <C-f> <Nop>
onoremap <C-f> <Nop>

nnoremap <C-u> <Nop>
inoremap <C-u> <Nop>
vnoremap <C-u> <Nop>
onoremap <C-u> <Nop>

nnoremap <C-d> <Nop>
inoremap <C-d> <Nop>
vnoremap <C-d> <Nop>
onoremap <C-d> <Nop>

nnoremap <C-y> <Nop>
inoremap <C-y> <Nop>
vnoremap <C-y> <Nop>
onoremap <C-y> <Nop>

nnoremap <C-e> <Nop>
inoremap <C-e> <Nop>
vnoremap <C-e> <Nop>
onoremap <C-e> <Nop>

nnoremap <C-h> <Nop>
inoremap <C-h> <Nop>
vnoremap <C-h> <Nop>
onoremap <C-h> <Nop>

nnoremap <C-j> <Nop>
inoremap <C-j> <Nop>
vnoremap <C-j> <Nop>
onoremap <C-j> <Nop>

nnoremap <C-r> <Nop>

nnoremap <C-c> <Nop>

" nnoremap <C-w> <Nop>
" inoremap <C-w> <Nop>
" vnoremap <C-w> <Nop>

" -----------------------------------------------------------------------------
" Mapped Key Bindings
" -----------------------------------------------------------------------------

" Set "<Leader>".
let mapleader = "\<Space>"

" Switch to the Normal Mode.
inoremap <A-q> <Esc>
vnoremap <A-q> <Esc>
cnoremap <A-q> <Esc>
onoremap <A-q> <Esc>

inoremap <A-Q> <Esc>
vnoremap <A-Q> <Esc>
cnoremap <A-Q> <Esc>
onoremap <A-Q> <Esc>

" Move the cursor up to the line above.
nnoremap h gk
vnoremap h gk
onoremap h gk

" Move the cursor down to the line below.
nnoremap k gj
vnoremap k gj
onoremap k gj

" Move the cursor left to the next character.
nnoremap j h
vnoremap j h
onoremap j h

" Move the cursor right to the previous character.
nnoremap l l
vnoremap l l
onoremap l l

" Scroll up half a screen.
nnoremap H <C-u>
vnoremap H <C-u>
onoremap H <C-u>

" Scroll down half a screen.
nnoremap K <C-d>
vnoremap K <C-d>
onoremap K <C-d>

" Move the cursor to the beginning of the line.
nnoremap J ^
vnoremap J ^
onoremap J ^

" Move the cursor to the end of the line.
nnoremap L $
vnoremap L $
onoremap L $

" Move the cursor forward to the beginning of the next token.
nnoremap b w
vnoremap b w
onoremap b w

" Move the cursor backward to the beginning of the previous token.
nnoremap B b
vnoremap B b
onoremap B b

" Move the cursor forward to the end of the next token.
nnoremap e e
vnoremap e e
onoremap e e

" Move the cursor backward to the end of the previous token.
nnoremap E ge
vnoremap E ge
onoremap E ge

" Delete (Kill) operations.
nnoremap d d
vnoremap d d
onoremap d d

nnoremap dd dd
vnoremap dd dd

nnoremap D D
vnoremap D D

" Join (Combine) the current line with the line below.
nnoremap c J
vnoremap c J

" Join (Combine) the current line with the line above.
nnoremap C <Up>J
vnoremap C J

" Switch to the Insert Mode before the cursor.
nnoremap i i

" Switch to the Insert Mode after the cursor.
nnoremap I a

" Switch to the Insert Mode at the beginning of the current line.
nnoremap a I

" Switch to the Insert Mode at the end of the current line.
nnoremap A A

" Leader for around (outer) text objects.
vnoremap o a
onoremap o a

" Open a new line below.
nnoremap o o<Esc>

" Open a new line above.
nnoremap O O<Esc>

" Redo.
nnoremap U <C-r>

" Leader for window commands.
" nnoremap <Leader>w <C-w>
" vnoremap <Leader>w <C-w>

" Switch focus to the upper window.
" nnoremap <A-w> <C-w>k
" vnoremap <A-w> <C-w>k

" nnoremap <Leader>ww <C-w>k
" vnoremap <Leader>ww <C-w>k

" Switch focus to the lower window.
" nnoremap <A-s> <C-w>j
" vnoremap <A-s> <C-w>j

" nnoremap <Leader>ws <C-w>j
" vnoremap <Leader>ws <C-w>j

" Switch focus to the left window.
" nnoremap <A-a> <C-w>h
" vnoremap <A-a> <C-w>h

" nnoremap <Leader>wa <C-w>h
" vnoremap <Leader>wa <C-w>h

" Switch focus to the right window.
" nnoremap <A-d> <C-w>l
" vnoremap <A-d> <C-w>l

" nnoremap <Leader>wd <C-w>l
" vnoremap <Leader>wd <C-w>l

" Move the current window to the very top.
" nnoremap <Up> <C-w>K
" vnoremap <Up> <C-w>K

" Move the current window to the very bottom.
" nnoremap <Down> <C-w>J
" vnoremap <Down> <C-w>J

" Move the current window to the very left.
" nnoremap <Left> <C-w>H
" vnoremap <Left> <C-w>H

" Move the current window to the very right.
" nnoremap <Right> <C-w>L
" vnoremap <Right> <C-w>L

" Break the current line and move to the next one.
nnoremap <CR>   i<CR><Esc><Right>
nnoremap <S-CR> a<CR><Esc><Right>

" Break the current line and stay on it.
inoremap <S-CR> <CR><Esc><Up>A

" Cut to the system clipboard.
nnoremap <C-x> "+x
vnoremap <C-x> "+x
cnoremap <C-x> "+x

" Copy to the system clipboard.
nnoremap <C-y> "+y
vnoremap <C-y> "+y
cnoremap <C-y> "+y

" Paste from the system clipboard.
nnoremap <C-p> "+p
vnoremap <C-p> "+p
cnoremap <C-p> "+p

" Write (Save) the whole buffer to the current file.
nnoremap <C-s> :w<CR>
inoremap <C-s> <Esc>:w<CR>a

nnoremap <A-s> :call <SID>SynStack()<CR>

" nnoremap <silent><S-BS> m`:silent +g/\m^\s*$/d<CR>``:nohlsearch<CR> nnoremap
" <silent><BS>   m`:silent -g/\m^\s*$/d<CR>``:nohlsearch<CR>

" nnoremap <silent><S-CR> :set paste<CR>m`o<Esc>``:set nopaste<CR>
" nnoremap <silent><CR>   :set paste<CR>m`O<Esc>``:set nopaste<CR>

" -----------------------------------------------------------------------------
" "dwm"
" -----------------------------------------------------------------------------

let g:dwm_map_keys = 0

nnoremap <Tab> <C-w>w
nnoremap <S-Tab> <C-w>W

nmap <C-l> <Plug>DWMRotateCounterclockwise
nmap <C-j> <Plug>DWMRotateClockwise

nmap <Leader>wn <Plug>DWMNew
nmap <Leader>wc <Plug>DWMClose
nmap <Leader>wf <Plug>DWMFocus

nmap <Leader>wg <Plug>DWMGrowMaster
nmap <Leader>ws <Plug>DWMShrinkMaster

" -----------------------------------------------------------------------------
" "MiniBufExplorer"
" -----------------------------------------------------------------------------

let g:miniBufExplModSelTarget = 1

noremap <Leader>b :MiniBufExplorer<CR>


" -----------------------------------------------------------------------------
" "NERDCommenter"
" -----------------------------------------------------------------------------

"noremap <C-/> :call NERDCommenterToggle

" -----------------------------------------------------------------------------
" "NERDTree"
" -----------------------------------------------------------------------------

"let g:NERDTreeMapOpenSplit  = 'h'
"let g:NERDTreeMapOpenVSplit = 'v'

"let g:NERDTreeMapToggleZoom   = 'tz'
"let g:NERDTreeMapToggleHidden = 'th'

"let g:NERDTreeMapOpenInTab       = ''
"let g:NERDTreeMapOpenInTabSilent = ''

"let g:NERDTreeMapQuit = ''

"let g:NERDTreeMapDeleteBookmark = 'k'

"let g:NERDTreeQuitOnOpen = 1
"let g:NERDTreeShowHidden = 0

"let g:NERDTreeWinPos  = 'right'
"let g:NERDTreeWinSize = 32

"let g:NERDTreeDirArrows = 1

"let g:NERDTreeChDirMode = 2

" let g:NERDTreeIgnore = ['^\./', '^\../']

nnoremap <Leader>t :NERDTreeToggle<CR>

" -----------------------------------------------------------------------------
" "fswitch"
" -----------------------------------------------------------------------------

nnoremap <silent> <Leader>s :w<CR>:FSHere<CR>

" nnoremap <silent> <Leader>sw :FSAbove<CR>
" nnoremap <silent> <Leader>ss :FSBelow<CR>
" nnoremap <silent> <Leader>sa :FSLeft<CR>
" nnoremap <silent> <Leader>sd :FSRight<CR>

" nnoremap <silent> <Leader>sW :FSSplitAbove<CR>
" nnoremap <silent> <Leader>sS :FSSplitBelow<CR>
" nnoremap <silent> <Leader>sA :FSSplitLeft<CR>
" nnoremap <silent> <Leader>sD :FSSplitRight<CR>

" -----------------------------------------------------------------------------
" "Tabular"
" -----------------------------------------------------------------------------

nnoremap <Leader>a= :Tabularize /=<CR>
vnoremap <Leader>a= :Tabularize /=<CR>

nnoremap <Leader>a: :Tabularize /:\zs<CR>
vnoremap <Leader>a: :Tabularize /:\zs<CR>

nnoremap <Leader>a? :Tabularize /[?:]<CR>
vnoremap <Leader>a? :Tabularize /[?:]<CR>

" -----------------------------------------------------------------------------
" "SameSyntaxMotion"
" -----------------------------------------------------------------------------

let g:SameSyntaxMotion_TextObjectMapping = 'g'

" Format syntax group.
nmap <Leader>fg gwag

" -----------------------------------------------------------------------------
" "Syntastic"
" -----------------------------------------------------------------------------

"let g:syntastic_java_javac_config_file_enabled = 1

" -----------------------------------------------------------------------------
" "delimitMate"
" -----------------------------------------------------------------------------

let g:delimitMate_excluded_regions = ''

let g:delimitMate_matchpairs = '(:),[:],{:}'

let g:delimitMate_balance_matchpairs = 1

let g:delimitMate_expand_cr    = 1
let g:delimitMate_expand_space = 1
" let g:delimitMate_excluded_ft  = 'tex'

imap <A-e> <Plug>delimitMateS-Tab
imap <A-E> <Plug>delimitMateS-Tab

" -----------------------------------------------------------------------------
" "SuperTab"
" -----------------------------------------------------------------------------

"let g:SuperTabDefaultCompletionType = 'context'

"let g:SuperTabNoCompleteAfter =
"\ ['^', '\s', ',', '(', ')', '[', ']', '{', '}', '<', '>', '''', '"']

"let g:SuperTabMappingForward  = '<C-Leader'
"let g:SuperTabMappingForward  = '<Tab>'
"let g:SuperTabMappingBackward = '<S-Tab>'

"let g:SuperTabLongestEnhanced  = 1
"let g:SuperTabLongestHighlight = 1

" -----------------------------------------------------------------------------
" "UltiSnips"
" -----------------------------------------------------------------------------

let g:UltiSnipsSnippetsDir        = '~/.vim/snippets/'
let g:UltiSnipsSnippetDirectories = ['UltiSnips', 'snippets']

" let g:UltiSnipsExpandTrigger       = '<C-CR>'
let g:UltiSnipsJumpForwardTrigger  = '<A-d>'
let g:UltiSnipsJumpBackwardTrigger = '<A-a>'


" -----------------------------------------------------------------------------
" "syntastic"
" -----------------------------------------------------------------------------

"let g:syntastic_check_on_open      = 1
"let g:syntastic_echo_current_error = 1
"let g:syntastic_enable_signs       = 1
"let g:syntastic_auto_jump          = 0
"let g:syntastic_auto_loc_list      = 2
"let g:syntastic_loc_list_height    = 5


" -----------------------------------------------------------------------------
" Custom Functions
" -----------------------------------------------------------------------------

" Show syntax highlighting groups for word under the cursor.
function! <SID>SynStack()
  if !exists('*synstack')
    return
  endif

  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" Restore cursor position, window position, and last search after running a
" command.
function! Preserve(command)
  " Save the last search.
  let search = @/

  " Save the current cursor position.
  let cursor_position = getpos('.')

  " Save the current window position.
  normal! H
  let window_position = getpos('.')
  call setpos('.', cursor_position)

  " Execute the command.
  execute a:command

  " Restore the last search.
  let @/ = search

  " Restore the previous window position.
  call setpos('.', window_position)
  normal! zt

  " Restore the previous cursor position.
  call setpos('.', cursor_position)
endfunction

" Re-indent the whole buffer.
function! Indent()
  call Preserve('normal gg=G')
endfunction

" Remove trailing whitespace in the whole buffer.
function! Trim()
  call Preserve('%s/\s\+$//e')
endfunction

let g:uncrustify_cfg_file_path =
    \ shellescape(fnamemodify('~/.uncrustify.cfg', ':p'))

function! Uncrustify(language)
  call Preserve(':silent %!uncrustify'
      \ . ' -q '
      \ . ' -l ' . a:language
      \ . ' -c ' . g:uncrustify_cfg_file_path)
endfunction

nnoremap <F8> :call Uncrustify("CPP")<CR>

function! AutoPEP8(pep8_passes)
  call Preserve(':silent %!autopep8'
      \ . ' -p ' . a:pep8_passes
      \ . ' '    . shellescape(expand('%:p')))
endfunction

let g:PythonTidy_cfg_file_path =
    \ shellescape(fnamemodify('~/.PythonTidy.xml', ':p'))

function! PythonTidy()
  call Preserve(':silent %!PythonTidy'
      \ . ' -u ' . g:PythonTidy_cfg_file_path)
endfunction

" -----------------------------------------------------------------------------
" Cuda highlighting support
" -----------------------------------------------------------------------------
au BufNewFile,BufRead *.cu set filetype=cpp
au BufNewFile,BufRead *.cuh set filetype=cpp

" -----------------------------------------------------------------------------
" Junk
" -----------------------------------------------------------------------------
" " Close the current buffer map <leader>bd :Bclose<cr>

" " Close all the buffers
" map <leader>ba :1,1000 bd!<cr>

" " Useful mappings for managing tabs
" map <leader>tn :tabnew<cr>
" map <leader>to :tabonly<cr>
" map <leader>tc :tabclose<cr>
" map <leader>tm :tabmove

" " Opens a new tab with the current buffer's path
" " Super useful when editing files in the same directory
" map <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/

" " Switch CWD to the directory of the open buffer
" map <leader>cd :cd %:p:h<cr>:pwd<cr>
