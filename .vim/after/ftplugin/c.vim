" -----------------------------------------------------------------------------
" Indentation
" -----------------------------------------------------------------------------

setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2
setlocal expandtab
setlocal cindent

" -----------------------------------------------------------------------------
" Wrapping
" -----------------------------------------------------------------------------

setlocal formatoptions=c,r,q,a

" -----------------------------------------------------------------------------
" Hooks
" -----------------------------------------------------------------------------

autocmd BufEnter *.h let b:fswitchdst = 'c'
autocmd BufEnter *.c let b:fswitchdst = 'h'

autocmd BufEnter *.h let b:fswitchlocs = 'reg:/include/src/'
autocmd BufEnter *.c let b:fswitchlocs = 'reg:/src/include/'

"autocmd BufWritePre <buffer> call Uncrustify('c')
