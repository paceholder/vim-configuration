" -----------------------------------------------------------------------------
" Indentation
" -----------------------------------------------------------------------------

setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2
setlocal expandtab
setlocal cindent



" Wrapping {{{

" setlocal formatoptions=c,r,q,a

" ------------------------------------------------------------------------------
" 1. Auto-wrap comments using `textwidth`;
" 2. Allow formatting of comments with `gq`;
" 3. Auto-insert comment leader after hitting `<CR>` in insert mode.

let &l:formatoptions =
    \ join(
    \   [
    \     'c',
    \     'q',
    \     'r',
    \   ],
    \   ','
    \ )

" ------------------------------------------------------------------------------
" }}} Wrapping




" -----------------------------------------------------------------------------
" Hooks
" -----------------------------------------------------------------------------

autocmd BufEnter *.hpp let b:fswitchdst = 'cpp,c++,cxx,cc,c'
autocmd BufEnter *.cpp let b:fswitchdst = 'hpp,h++,hxx,hh,h'

autocmd BufEnter *.h++ let b:fswitchdst = 'c++,cpp,cxx,cc,c'
autocmd BufEnter *.c++ let b:fswitchdst = 'h++,hpp,hxx,hh,h'

autocmd BufEnter *.hxx let b:fswitchdst = 'cxx,cpp,c++,cc,c'
autocmd BufEnter *.cxx let b:fswitchdst = 'hxx,hpp,h++,hh,h'

autocmd BufEnter *.hh  let b:fswitchdst = 'cc,cpp,c++,cxx,c'
autocmd BufEnter *.cc  let b:fswitchdst = 'hh,hpp,h++,hxx,h'

autocmd BufEnter *.h   let b:fswitchdst = 'cpp,c++,cxx,cc,c'
autocmd BufEnter *.c   let b:fswitchdst = 'hpp,h++,hxx,hh,h'

autocmd BufEnter *.hpp,*.h++,*.hxx,*.hh,*.h let b:fswitchlocs =
    \ 'reg:/include/src/'
autocmd BufEnter *.cpp,*.c++,*.cxx,*.cc,*.c let b:fswitchlocs =
    \ 'reg:/src/include/'

"autocmd FileWritePre <buffer> call Uncrustify('cpp')
