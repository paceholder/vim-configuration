" -----------------------------------------------------------------------------
" Indentation
" -----------------------------------------------------------------------------

setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal expandtab

" -----------------------------------------------------------------------------
" Wrapping
" -----------------------------------------------------------------------------

setlocal formatoptions=c,r,q,a

" -----------------------------------------------------------------------------
" Hooks
" -----------------------------------------------------------------------------

nnoremap <C-s> :w<CR>:call AutoPEP8(1000)<CR>:w<CR>
inoremap <C-s> <Esc>:w<CR>:call AutoPEP8(1000)<CR>:w<CR>a

" autocmd BufWritePre <buffer> call AutoPEP8(1000)

" autocmd BufWritePre <buffer> call PythonTidy()

" autocmd BufWritePre <buffer> call Indent()
