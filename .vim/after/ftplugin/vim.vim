" -----------------------------------------------------------------------------
" Indentation
" -----------------------------------------------------------------------------

setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2
setlocal expandtab

" Amount of indent for a continuation line (the one that starts with a backward
" slash "\") in Vim scripts.
let g:vim_indent_cont = &shiftwidth * 2

" -----------------------------------------------------------------------------
" Wrapping
" -----------------------------------------------------------------------------

setlocal formatoptions=c,r,q,a

" -----------------------------------------------------------------------------
" Hooks
" -----------------------------------------------------------------------------

autocmd BufWritePre <buffer> call Indent()
