syntax keyword cTodo contained NOTE TODO HACK BUG FIXME

syntax match cMyParen   "?=("         contains=cParen,cCppParen
syntax match cMyFuncion "\w\+\s*(\@=" contains=cMyParen

syntax match cMyScopeResolution "::"
syntax match cMyScope           "\w\+\s*::" contains=cMyScopeResolution

highlight default link cMyFuncion Function
highlight default link cMyScope   Function
